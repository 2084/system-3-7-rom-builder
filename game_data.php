<?php
/**
 * Datafile for ROM Utility to build a collection of Williams System 3-7 Pinball ROMs into a single ROM file.
 * ROMs are in reverse production date order as the default state for the DIP switches with pull ups is high.
 * No ROMs are included in this repository though they should all be freely available online.
 * 
 * All custom ROMs were created using patches from Olivers excellent work here: http://www.pinball4you.ch/okaegi/pro_soft.html
 * 
 * PCB Revsion 1.0
 * November 2021
 * 
 * Dave Langley
 * https://www.robotron-2084.co.uk/
 * https://gitlab.com/2084/system-3-7-rom-builder
 */

$game_data = [
    // Test ROMs (4)
    ['name' => 'Test ROM',                         'id' => 0,   32 => true,  'src' => 'test',            'roms' => [14 => false,          17 => 'wtstic17.532', 20 => false,          26 => false]],
    ['name' => 'Leon System 3-6 Test ROM',         'id' => 0,   32 => true,  'src' => 'leon',            'roms' => [14 => false,          17 => 'leontest.716', 20 => false,          26 => false]],
    ['name' => 'Leon System 7 ROM',                'id' => 0,   32 => true,  'src' => 'leon',            'roms' => [14 => false,          17 => 'leontest.532', 20 => false,          26 => false]],
    ['name' => 'Andre Test ROM',                   'id' => 0,   32 => true,  'src' => 'andre',           'roms' => [14 => false,          17 => 'testROM.716',  20 => false,          26 => false]],

    // Padding to make it up to 64 games
    ['name' => 'Padding',                          'id' => 0,   32 => false, 'src' => '',                'roms' => [14 => false,          17 => false,          20 => false,          26 => false]],
    ['name' => 'Padding',                          'id' => 0,   32 => false, 'src' => '',                'roms' => [14 => false,          17 => false,          20 => false,          26 => false]],
    ['name' => 'Padding',                          'id' => 0,   32 => false, 'src' => '',                'roms' => [14 => false,          17 => false,          20 => false,          26 => false]],
    ['name' => 'Padding',                          'id' => 0,   32 => false, 'src' => '',                'roms' => [14 => false,          17 => false,          20 => false,          26 => false]],
    ['name' => 'Padding',                          'id' => 0,   32 => false, 'src' => '',                'roms' => [14 => false,          17 => false,          20 => false,          26 => false]],
    ['name' => 'Padding',                          'id' => 0,   32 => false, 'src' => '',                'roms' => [14 => false,          17 => false,          20 => false,          26 => false]],
    ['name' => 'Padding',                          'id' => 0,   32 => false, 'src' => '',                'roms' => [14 => false,          17 => false,          20 => false,          26 => false]],
    ['name' => 'Padding',                          'id' => 0,   32 => false, 'src' => '',                'roms' => [14 => false,          17 => false,          20 => false,          26 => false]],
    ['name' => 'Padding',                          'id' => 0,   32 => false, 'src' => '',                'roms' => [14 => false,          17 => false,          20 => false,          26 => false]],
    ['name' => 'Padding',                          'id' => 0,   32 => false, 'src' => '',                'roms' => [14 => false,          17 => false,          20 => false,          26 => false]],

    // System 7 Games (19)
    ['name' => 'Star Light (L-1)',                 'id' => 530, 32 => true,  'src' => 'star-light',      'roms' => [14 => 'IC14.532',     17 => 'IC17.532',     20 => 'IC20.532',     26 => false]],
    ['name' => 'Laser Cue (L-2)',                  'id' => 520, 32 => true,  'src' => 'laser-cue',       'roms' => [14 => 'IC14.716',     17 => 'IC17.532',     20 => 'IC20.716',     26 => 'IC26.716']],
    ['name' => 'Firepower II (L-2)',               'id' => 521, 32 => true,  'src' => 'firepower2',      'roms' => [14 => 'IC14.716',     17 => 'IC17.532',     20 => 'IC20.716',     26 => 'IC26.716']],
    ['name' => 'Joust (L-2)',                      'id' => 519, 32 => true,  'src' => 'joust',           'roms' => [14 => 'IC14.716',     17 => 'IC17.532',     20 => 'IC20.716',     26 => 'IC26.716']],
    ['name' => 'Time Fantasy (L-5)',               'id' => 515, 32 => true,  'src' => 'time-fantasy',    'roms' => [14 => 'IC14.716',     17 => 'IC17.532',     20 => 'IC20.716',     26 => 'IC26.716']],
    ['name' => 'Rat Race (L-1)',                   'id' => 527, 32 => true,  'src' => 'rat-race',        'roms' => [14 => 'IC14.532',     17 => 'IC17.532',     20 => 'IC20.532',     26 => false]],
    ['name' => 'Defender (L-4)',                   'id' => 517, 32 => true,  'src' => 'defender',        'roms' => [14 => 'IC14.532',     17 => 'IC17.532',     20 => 'IC20.532',     26 => false]],
    ['name' => 'Warlok (L-3)',                     'id' => 516, 32 => true,  'src' => 'warlock',         'roms' => [14 => 'IC14.716',     17 => 'IC17.532',     20 => 'IC20.716',     26 => 'IC26.716']],
    ['name' => 'Varkon (L-1)',                     'id' => 512, 32 => true,  'src' => 'varkon',          'roms' => [14 => 'IC14.716',     17 => 'IC17.532',     20 => 'IC20.716',     26 => 'IC26.716']],
    ['name' => 'Cosmic Gunfight (L-1)',            'id' => 502, 32 => true,  'src' => 'cosmic-gunfight', 'roms' => [14 => 'IC14.716',     17 => 'IC17.532',     20 => 'IC20.716',     26 => 'IC26.716']],
    ['name' => 'Thunderball (L-1)',                'id' => 508, 32 => true,  'src' => 'thunderball',     'roms' => [14 => 'IC14.532',     17 => 'IC17.532',     20 => 'IC20.532',     26 => false]],
    ['name' => 'Hyperball (L-6 Hy Score Save)',    'id' => 509, 32 => true,  'src' => 'hyperball',       'roms' => [14 => 'IC14.532',     17 => 'IC17.532',     20 => 'IC20_L6.532',  26 => false]],
    ['name' => 'Hyperball (L-4)',                  'id' => 509, 32 => true,  'src' => 'hyperball',       'roms' => [14 => 'IC14.532',     17 => 'IC17.532',     20 => 'IC20.532',     26 => false]],
    ['name' => 'Barracora (L-1)',                  'id' => 510, 32 => true,  'src' => 'barracora',       'roms' => [14 => 'IC14.716',     17 => 'IC17.532',     20 => 'IC20.716',     26 => 'IC26.716']],
    ['name' => 'Solar Fire (L-2)',                 'id' => 507, 32 => true,  'src' => 'solar-fire',      'roms' => [14 => 'IC14.716',     17 => 'IC17.532',     20 => 'IC20.716',     26 => 'IC26.716']],
    ['name' => 'Pharaoh (L-2)',                    'id' => 504, 32 => true,  'src' => 'pharoah',         'roms' => [14 => 'IC14.716',     17 => 'IC17.532',     20 => 'IC20.716',     26 => 'IC26.716']],
    ['name' => 'Jungle Lord (l-2)',                'id' => 503, 32 => true,  'src' => 'jungle-lord',     'roms' => [14 => 'IC14.716',     17 => 'IC17.532',     20 => 'IC20.716',     26 => 'IC26.716']],
    ['name' => 'Black Knight (L-4)',               'id' => 500, 32 => true,  'src' => 'black-knight',    'roms' => [14 => 'IC14.716',     17 => 'IC17.532',     20 => 'IC20.716',     26 => 'IC26.716']],
    ['name' => 'Firepower (Oliver v380 System 7)', 'id' => 497, 32 => false, 'src' => 'okaegi',          'roms' => [14 => 'f7ic14c7.716', 17 => 'f7ic17c7.532', 20 => 'f7ic20c7.716', 26 => 'f7ic26c7.716']],

    // System 6A Games (3)
    ['name' => 'Alien Poker (Oliver v320)',        'id' => 501, 32 => false, 'src' => 'okaegi',          'roms' => [14 => 'al32ic14.732', 17 => 'al32ic17.716', 20 => 'al32ic20.716', 26 => false]],
    ['name' => 'Alien Poker (L-6)',                'id' => 501, 32 => true,  'src' => 'alien-poker',     'roms' => [14 => 'GAMEROM6.716', 17 => 'GREEN2.716',   20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Algar (L-1)',                      'id' => 499, 32 => true,  'src' => 'algar',           'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2.716',   20 => 'GREEN1.716',   26 => false]],

    // System 6 Games (18)
    ['name' => 'Scorpion (/10 Scoring)',           'id' => 494, 32 => false, 'src' => 'scorpion',        'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2A.716',  20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Scorpion (L-1)',                   'id' => 494, 32 => true,  'src' => 'scorpion',        'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2.716',   20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Blackout (/10 Scoring)',           'id' => 495, 32 => false, 'src' => 'blackout',        'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2A.716',  20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Blackout (L-1)',                   'id' => 495, 32 => true,  'src' => 'blackout',        'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2.716',   20 => 'GREEN1.716',   26 => false]],
    // ['name' => 'Firepower (Oliver v320 7-digit)',  'id' => 497, 32 => false, 'src' => 'okaegi',          'roms' => [14 => 'f7ic14b7.716', 17 => 'f7ic17b7.532', 20 => 'f7ic20b7.716', 26 => false]],
    ['name' => 'Firepower (Oliver v31 6-digit)',   'id' => 497, 32 => false, 'src' => 'okaegi',          'roms' => [14 => 'f6p7ic14.732', 17 => 'f6p7ic17.716', 20 => 'f6p7ic20.716', 26 => false]],
    ['name' => 'Firepower (L-6 /10 Scoring)',      'id' => 497, 32 => false, 'src' => 'firepower',       'roms' => [14 => 'FPCOMBO6.732', 17 => 'GREEN2A.716',  20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Firepower (L-6)',                  'id' => 497, 32 => true,  'src' => 'firepower',       'roms' => [14 => 'FPCOMBO6.732', 17 => 'GREEN2.716',   20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Firepower (L-2 /10 Scoring)',      'id' => 497, 32 => false, 'src' => 'firepower',       'roms' => [14 => 'FPCOMBO2.732', 17 => 'GREEN2A.716',  20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Firepower (L-2)',                  'id' => 497, 32 => true,  'src' => 'firepower',       'roms' => [14 => 'FPCOMBO2.732', 17 => 'GREEN2.716',   20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Laser Ball (/10 Scoring)',         'id' => 493, 32 => false, 'src' => 'laser-ball',      'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2A.716',  20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Laser Ball (L-2)',                 'id' => 493, 32 => true,  'src' => 'laser-ball',      'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2.716',   20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Gorgar (/10 Scoring)',             'id' => 496, 32 => false, 'src' => 'gorgar',          'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2A.716',  20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Gorgar (L-1)',                     'id' => 496, 32 => true,  'src' => 'gorgar',          'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2.716',   20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Time Warp (/10 Scoring)',          'id' => 489, 32 => false, 'src' => 'time-warp',       'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2A.716',  20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Time Warp (L-2)',                  'id' => 489, 32 => true,  'src' => 'time-warp',       'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2.716',   20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Tri Zone (/10 Scoring)',           'id' => 487, 32 => false, 'src' => 'tri-zone',        'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2A.716',  20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Tri Zone (L-1)',                   'id' => 487, 32 => true,  'src' => 'tri-zone',        'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2.716',   20 => 'GREEN1.716',   26 => false]],

    // System 4 Games (5)
    ['name' => 'Stellar Wars (L-2)',               'id' => 490, 32 => false, 'src' => 'stellar-wars',    'roms' => [14 => 'GAMEROM.716',  17 => 'YELLOW2.716',  20 => 'YELLOW1.716',  26 => false]],
    ['name' => 'Flash (/10 Scoring)',              'id' => 486, 32 => false, 'src' => 'flash',           'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2A.716',  20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Flash (L-1)',                      'id' => 486, 32 => false, 'src' => 'flash',           'roms' => [14 => 'GAMEROM.716',  17 => 'GREEN2.716',   20 => 'GREEN1.716',   26 => false]],
    ['name' => 'Phoenix (L-1 Switch Bugfix)',      'id' => 485, 32 => false, 'src' => 'phoenix',         'roms' => [14 => 'GAMEROM2.716', 17 => 'WHITE2.716',   20 => 'WHITE1.716',   26 => false]],
    ['name' => 'Phoenix (L-1)',                    'id' => 485, 32 => false, 'src' => 'phoenix',         'roms' => [14 => 'GAMEROM2.716', 17 => 'WHITE2.716',   20 => 'WHITE1.716',   26 => false]],
    ['name' => 'Pokerino (L-1)',                   'id' => 488, 32 => false, 'src' => 'pokerino',        'roms' => [14 => 'GAMEROM.716',  17 => 'WHITE2.716',   20 => 'WHITE1.716',   26 => false]],

    // System 3 Games (5)
    ['name' => 'Disco Fever (L-1)',                'id' => 483, 32 => false, 'src' => 'disco-fever',     'roms' => [14 => 'GAMEROM.716',  17 => 'WHITE2.716',   20 => 'WHITE1.716',   26 => false]],
    ['name' => 'Contact (L-1)',                    'id' => 482, 32 => false, 'src' => 'contact',         'roms' => [14 => 'GAMEROM.716',  17 => 'WHITE2.716',   20 => 'WHITE1.716',   26 => false]],
    ['name' => 'World Cup (L-1)',                  'id' => 481, 32 => false, 'src' => 'world-cup',       'roms' => [14 => 'GAMEROM.716',  17 => 'WHITE2.716',   20 => 'WHITE1.716',   26 => false]],
    ['name' => 'Lucky Seven (L-1)',                'id' => 480, 32 => false, 'src' => 'lucky-seven',     'roms' => [14 => 'GAMEROM.716',  17 => 'WHITE2.716',   20 => 'WHITE1.716',   26 => false]],
    ['name' => 'Hot Tip (L-1)',                    'id' => 477, 32 => false, 'src' => 'hot-tip',         'roms' => [14 => 'GAMEROM.716',  17 => 'WHITE2.716',   20 => 'WHITE1.716',   26 => false]],
];
// 