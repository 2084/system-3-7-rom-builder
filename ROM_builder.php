<?php
/**
 * ROM Utility to build a collection of Williams System 3-7 Pinball ROMs into a single ROM file to be bank switched
 * so that we can easily swap between games without the need to program EPROMs.
 * 
 * Configure the games you want to include in the combined ROM in the file game_data.php
 *
 * Usage: from a MacOS, Linux, Unix or Windows (with PHP installed) command prompt type:
 * php ROM_builder.php
 *
 * The script will output two files, the combined ROM and a csv file detailing the games that were included and the DIP switch settings.
 * If the ROM extenstion is .incomplete then check the output or the game_data file for errors or not enough games.
 * 
 * DIP Switches:
 * 1  - EPROM A19 Select
 * 2  - EPROM A18 Select
 * 3  - EPROM A17 Select
 * 4  - EPROM A16 Select
 * 5  - EPROM A15 Select
 * 6  - EPROM A14 Select
 * 
 * PCB Revsion 1.0
 * November 2021
 * 
 * Dave Langley
 * https://www.robotron-2084.co.uk/
 * https://gitlab.com/2084/system-3-7-rom-builder
 */

$version = '1.0';
$filename_version = str_replace('.', '_', $version);

echo 'Starting System 3-7 Combined ROM Build ' . $version  . '...' . PHP_EOL . PHP_EOL;

// Load game data
(@include_once ('game_data.php')) OR die('Error: File game_data.php not found!' . PHP_EOL . PHP_EOL);
if (!isset($game_data) || !(is_array($game_data))) die ('Error: File game_data.php does not contain valid game_data array!' . PHP_EOL . PHP_EOL);

// Check for options
$thirytwo = isset($argv[1]) && $argv[1] == '-32' ? true : false;

// Set up some variables and a 8k fill file containing FF
$multigame_rom = $fill_file_2k = $fill_file_4k = $fill_file_8k = $fill_file_16k = $csv_file = '';
for ($i=0; $i < 2048 ; $i++) { $fill_file_2k .= chr(255); }
for ($i=0; $i < 4096 ; $i++) { $fill_file_4k .= chr(255); }
for ($i=0; $i < 8192 ; $i++) { $fill_file_8k .= chr(255); }
for ($i=0; $i < 16384 ; $i++) { $fill_file_16k .= chr(255); }

// Loop over all games
$game_count = 0;
foreach ($game_data as $game_index => $game) {
    // Bail if this is a 32 game build and this game is not flagged for inclusion
    if ($thirytwo && !$game[32]) continue;

    // Increment game count
    $game_count++;

    // Initialise variables
    $roms_valid = true;
    $game_combined_rom = '';
    $source_path = __DIR__ . '/source_roms/' . ($game['src'] ? $game['src'] . '/' : '');
    $target_filename = __DIR__ . '/combined_roms/' . preg_replace('/[^A-Za-z0-9-]+/', '_', strtolower($game['name'])) . '.2764';

    // Check all listed ROMs exist and load them into memory
    foreach ($game['roms'] as $rom_location => $rom_name) {
        if ($rom_name && file_exists($source_path . $rom_name)) {
            $roms[$rom_location] = file_get_contents($source_path . $rom_name);
        } else {
            if ($rom_name) {
                echo '[' . str_pad($game_count, 2, '0', STR_PAD_LEFT)  .'] ERROR: Failed to load file IC' . $rom_location . ' ' . $rom_name . ' for ' . $game['name'] . PHP_EOL;
            }
            $roms[$rom_location] = '';
        }
    }

    // Build the ROM file, different generations have different requirements
    // The bultigame ROM will cover the address range 4000-7FFF
    $game_combined_rom = $fill_file_4k; // 4000-4FFF is never used
    if (strlen($roms[14]) == 2048 && strlen($roms[17]) == 4096 && strlen($roms[20]) == 2048 && strlen($roms[26]) == 2048) {
        // 2K + 4K + 2K + 2K
        $game_combined_rom .= $fill_file_2k;    // 5000-57FF
        $game_combined_rom .= $roms[26];        // 5800-5FFF
        $game_combined_rom .= $roms[14];        // 6000-67FF
        $game_combined_rom .= $roms[20];        // 6800-6FFF
        $game_combined_rom .= $roms[17];        // 7000-7FFF
    } else if (strlen($roms[14]) == 4096 && strlen($roms[17]) == 4096 && strlen($roms[20]) == 4096 && strlen($roms[26]) == 0) {
        // 4K + 4K + 4K + 0K
        $game_combined_rom .= $roms[20];        // 5000-5FFF
        $game_combined_rom .= $roms[14];        // 6000-6FFF
        $game_combined_rom .= $roms[17];        // 7000-7FFF
    } else if (strlen($roms[14]) == 2048 && strlen($roms[17]) == 2048 && strlen($roms[20]) == 2048 && strlen($roms[26]) == 0) {
        // 2K + 2K + 2K + 0K
        $game_combined_rom .= $fill_file_4k;    // 5000-5FFF
        $game_combined_rom .= $roms[14];        // 6000-67FF
        $game_combined_rom .= $fill_file_2k;    // 6800-6FFF
        $game_combined_rom .= $roms[20];        // 7000-77FF 
        $game_combined_rom .= $roms[17];        // 7800-7FFF
    } else if (strlen($roms[14]) == 4096 && strlen($roms[17]) == 2048 && strlen($roms[20]) == 2048 && strlen($roms[26]) == 0) {
        // 4K + 2K + 2K + 0K - most likely firepower
        $game_combined_rom .= $fill_file_4k;    // 5000-5FFF
        $game_combined_rom .= $roms[14];        // 6000-6FFF
        $game_combined_rom .= $roms[20];        // 7000-77FF 
        $game_combined_rom .= $roms[17];        // 7800-7FFF
   } else if (strlen($roms[14]) == 0 && strlen($roms[17]) == 4096 && strlen($roms[20]) == 0 && strlen($roms[26]) == 0) {
        // 0K + 4K + 0K + 0K - 4k test ROM 
        $game_combined_rom .= $fill_file_4k;    // 5000-5FFF
        $game_combined_rom .= $fill_file_4k;    // 6000-6FFF
        $game_combined_rom .= $roms[17];        // 7000-7FFF
   } else if (strlen($roms[14]) == 0 && strlen($roms[17]) == 2048 && strlen($roms[20]) == 0 && strlen($roms[26]) == 0) {
        // 0K + 2K + 0K + 0K - 2K test ROM
        $game_combined_rom .= $fill_file_4k;    // 5000-5FFF
        $game_combined_rom .= $fill_file_4k;    // 6000-6FFF
        $game_combined_rom .= $fill_file_2k;    // 7000-77FF 
        $game_combined_rom .= $roms[17];        // 7800-7FFF
    } else if (strlen($roms[14]) == 0 && strlen($roms[17]) == 0 && strlen($roms[20]) == 0 && strlen($roms[26]) == 0) {
        // 0K + 0K + 0K + 0K - Empty ROMs for padding
        $game_combined_rom .= $fill_file_4k;    // 5000-5FFF
        $game_combined_rom .= $fill_file_4k;    // 6000-6FFF
        $game_combined_rom .= $fill_file_4k;    // 7000-7fFF 
    } else {
        echo '[' . str_pad($game_count, 2, '0', STR_PAD_LEFT)  .'] ERROR: Failed to parse ROMs for ' . $game['name'] . PHP_EOL;
        $game_combined_rom .= $fill_file_4k;    // 5000-5FFF
        $game_combined_rom .= $fill_file_4k;    // 6000-6FFF
        $game_combined_rom .= $fill_file_4k;    // 7000-7fFF 
    }

    if (strlen($game_combined_rom) == 16384) {
        echo '[' . str_pad($game_count, 2, '0', STR_PAD_LEFT)  .'] Parsed ROMs for ' . $game['name'] . PHP_EOL;
    } else {
        echo '[' . str_pad($game_count, 2, '0', STR_PAD_LEFT)  .'] ERROR: Parsed ROMs incorrect size for ' . $game['name'] . PHP_EOL;   
    }

    // Add this game to multigame ROM
    $multigame_rom .= $game_combined_rom;

    // Save game 27128 ROM
    $result = file_put_contents($target_filename, $game_combined_rom);
    if (!$result) {
        echo '[' . str_pad($game_count, 2, '0', STR_PAD_LEFT)  .'] Failed to save file ' . $target_filename . PHP_EOL;
    }
}

// Set file extension
$extension = 'incomplete';
if (strlen($multigame_rom) == 524288) {
    $extension ='27C040';
} else if (strlen($multigame_rom) == 1048576) {
    $extension ='27C080';
}

// Save multigame ROM
$filename = __DIR__ . "/multigame_roms/system_3_7_multigame_rom_v{$filename_version}.{$extension}";
$result = file_put_contents($filename, $multigame_rom);
if ($result) {
    echo PHP_EOL . 'Saved multi-game file ' . $filename . PHP_EOL;
} else {
    echo PHP_EOL . 'Failed to save multi-game file ' . $filename . PHP_EOL;
}

// Create csv file
$game_index = 0;
foreach (array_reverse($game_data) as $game) {
    // Bail if this is a 32 game build and this game is not flagged for inclusion
    if ($thirytwo && !$game[32]) continue;

    if ($game['name'] != 'Padding') {
        $csv_file .= $game_index + 1 . ',' . '"' . $game['name'] . '","\''
                  .  (($game_index & 32) == 32 ? '1' : '0')
                  .  (($game_index & 16) == 16 ? '1' : '0')
                  .  (($game_index &  8) ==  8 ? '1' : '0')
                  .  (($game_index &  4) ==  4 ? '1' : '0')
                  .  (($game_index &  2) ==  2 ? '1' : '0')
                  .  (($game_index &  1) ==  1 ? '1' : '0')
                  .  '",' . PHP_EOL;
    }

    // Increment the game_index
    $game_index++;
}

// Save csv file
$filename = __DIR__ . "/multigame_roms/system_3_7_multigame_rom_names_v{$filename_version}_{$extension}.csv";
$result = file_put_contents($filename, $csv_file);
if ($result) {
    echo 'Saved CSV file ' . $filename . PHP_EOL;
} else {
    echo 'Failed to save CSV file ' . $filename . PHP_EOL;
}

echo PHP_EOL . 'Done!' . PHP_EOL;
